#pragma once
#include <cassert>
#include <iostream>
#include <fstream>
#include "HSA_device.hpp"

class HSA_kernel{
	public:
    HSA_kernel(HSA_device *);
    virtual ~HSA_kernel();
    uint64_t load_hsaco_file(const char *, const char *);
    virtual void enqueue(hsa_signal_t*) = 0;
    virtual int verify_output(int spotcheck=0) = 0;

  protected:
    HSA_device *device;
    hsa_status_t hsa_status;
    uint64_t kernel_object;

    void gpu_allocate(void **, int, void * = NULL);
    void host_allocate(void **, int, void * = NULL);


    void sync_copy_gpu_to_host(void*, void*, int);
    void sync_copy_host_to_gpu(void*, void*, int);
};

