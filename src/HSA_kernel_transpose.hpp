#pragma once
#include "HSA_kernel.hpp"

#define N_ITER 32768
#define N_ELEM 2048

class HSA_kernel_transpose : public HSA_kernel {
	public:
		HSA_kernel_transpose(HSA_device *device);
		~HSA_kernel_transpose();
		void enqueue(hsa_signal_t*);

		int copy_output_data();
		int verify_output(int spotcheck = 0);
		void allocate_hsa_memory(void **);
		void initialize_memory();
		void **get_hsa_memory();

		char *input;
		char *output;

	    unsigned int* input_buffer;
	    unsigned int* output_buffer;
	    void *kernarg_address;
};
