#pragma once
#include <list>
#include <cassert>
#include <sys/time.h>
#include <unistd.h>

#include "hsa/hsa.h"
#include "hsa/hsa_ext_finalize.h"
#include "hsa/hsa_ext_amd.h"
#include "HSA_kernel_presum.hpp"
#include "HSA_kernel_N2.hpp"
#include "HSA_kernel_transpose.hpp"
#include "HSA_device.hpp"

class HSA_factory{
    public:
        hsa_status_t hsa_status;
        HSA_factory(int num_gpus);
        ~HSA_factory();
        int enqueue();

        char *input_data;
        int *presum;
        int num_gpus;

    private:
        std::list<HSA_kernel*> kernels;
        std::list<HSA_device*> devices;

};
